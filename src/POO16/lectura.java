package POO16;

import java.io.*;
public class lectura
{
	private BufferedReader obb=new BufferedReader 
			(new InputStreamReader(System.in));
	public byte LeerByte(String msj)
	{
		byte var=0;
		try
		{
			System.out.print(msj+" ");
			var=Byte.parseByte(obb.readLine().trim());
		}
		catch(NumberFormatException e)
		{
			var=LeerByte(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	public short LeerShort(String msj)
	{
		short var=0;
		try
		{
			System.out.print(msj+" ");
			var=Short.parseShort(obb.readLine().trim());
		}
		catch(NumberFormatException e)
		{
			var=LeerShort(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;	
	}
	public int LeerInt(String msj)
	{
		int var=0;
		try
		{
			System.out.print(msj+" ");
			var=Integer.parseInt(obb.readLine().trim());
		}
		catch(NumberFormatException e)
		{
			var=LeerInt(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	public long LeerLong(String msj)
	{
		long var=0;
		try
		{
			System.out.print(msj+" ");
			var=Long.parseLong(obb.readLine().trim());
		}
		catch(NumberFormatException e)
		{
			var=LeerLong(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	public float LeerFloat(String msj)
	{
		float var=0;
		try
		{
			System.out.print(msj+" ");
			var=Float.parseFloat(obb.readLine().trim());
		}
		catch(NumberFormatException e)
		{
			var=LeerFloat(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	public double LeerDouble(String msj)
	{
		double var=0;
		try
		{
			System.out.print(msj+" ");
			var=Double.parseDouble(obb.readLine().trim());
		}
		catch(NumberFormatException e)
		{
			var=LeerDouble(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	public char LeerChar(String msj)
	{
		char var=' ';
		try
		{
			System.out.print(msj+" ");
			var=obb.readLine().trim().charAt(0);
		}
		catch(NumberFormatException e)
		{
			var=LeerChar(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	public String LeerString(String msj)
	{
		String var=" ";
		try
		{
			System.out.print(msj+" ");
			var=obb.readLine().trim();
		}
		catch(NumberFormatException e)
		{
			var=LeerString(msj);
		}
		catch(IOException e)
		{
			System.out.println("Error en el dispositivo de entrada...");
		}
		return var;
		
	}
	
}
