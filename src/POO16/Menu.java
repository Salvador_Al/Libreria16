package POO16;

public class Menu
{
	private String tit, menu[];
	private int ind;
	
	public Menu(String tit, String menu[])
	{
		this.menu=new String[menu.length+1];
		this.tit=tit.toUpperCase();
		
		for(ind=0; ind<menu.length; ind++)
			this.menu[ind]=menu[ind].toUpperCase();
		this.menu[ind]="Salir";
	}
	
	private void Mostrar()
	{
		System.out.println(tit);
		for(ind=0; ind<menu.length; ind++)
			System.out.println((ind+1)+") "+menu[ind]);
	}
	public int Opcion()
	{
		int op;
		lectura obl=new lectura();
		do
		{
			this.Mostrar();
			op=obl.LeerInt("Cual es tu opcion?");
		}
		while(op<1 || op>menu.length);
		return op;
	}
	public int Salir()
	{
		return menu.length;
	}
}